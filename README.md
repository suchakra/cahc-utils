# Research projects at CAHC

- This repository is a place holder for research activities of CAHC
- This is expected to grow over time
- The code needs a lot of cleanup and sensible documentation

## datasets

- Area to hold inoput and out csvs across all CAHC projects
- Data is scattered now, overtime all data will should move here

## detect_silence

- Placeholder project to detect silence in audio streams
- May be useful to index/split/chink audio files

## lagadha

- Code/Docs to understand and compute concepts and algo in Lagadha Vedanga Jyotisha

## mars-retro

- Explore mars-retro paths and co-relate with shapes in VGJ

## dp - darsha-poornamasa  

- Some analytics darsha-poornamasa
- eclipses in jaipur/delhi/kuru area areoun 1400bce
- utilities to plot lunar standstill`

## moon_rohini

- data to help analyse proximity of moon with nakshatras
- moon is said to be closest to rohini
  
## music

- utilities to synthesize music based on melakarta encoding

## nakshatra_sky_culture

- utilities to add indic skyculture to stellarium
- vedic_sky_culture could be of end user interest

## parippadal

- utilities to check position of krtikka, agastya and planets in madurai
- between bce 500 to ce 600 to match astro info in parippadal

## planetplot

- utilities for compute position of planets analytically
- opposed to scraping from stellarium - a slow process

## scrape

- scrapes 10 mandala pages from [<https://sa.m.wikisource.org/wiki/ऋग्वेदः>ऋग्वेदः]
- extracts poem and sayana bhashya for each suktha of each mandala
- scrapes skanda purana from [<https://sa.m.wikisource.org/wiki/स्कन्दपुराणम्]

## stel_scripts

- collection  stellarium scripts to do one-off tasks
- tour-of-nakshatras might be end user relevant than others
  
## vaidika-ganakam वैदिकगणकम्

- There are rules that govern the rate at which Rg Veda is orally recited.  
- This project aims to code those rules and look for embbeded time measurement mechanisms in the Rg Veda.

## vgj-adityachaara-rtusvabhaava

- plot information in adityachaara and rtusvabhaava chapters of vgj
- from ac chapter, plot naks error for many years wrt to dha at given 270 epoch
- for rs chapter, plot the rtu, vedic and loukia masa over nakshatra backdrop
